import React, { Component } from 'react'
import ReactDom from 'react-dom'
import YTSearch from 'youtube-api-search'

import SearchBar from './components/search_bar'
import VideoList from './components/video_list'
import VideoDetail from './components/video_detail'

const API_KEY = 'AIzaSyBx25YaQFYzvYmwENTevU94A8TYw_NEXTQ'

class App extends Component {
  constructor(props){

    super(props);

    this.state = { 
      videos: [],
      selectedVideo: null

    };

    this.videoSearch('react js');
  }
    
  videoSearch(term){  
    YTSearch({ key: API_KEY, term: term }, (videos) => {
      this.setState({ 

        videos: videos,
        selectedVideo: videos[0]

        }) //resutl of this line this.setState( { videos: videos })
      
      })
    }

  render(){
    return (
      <div>
        <SearchBar onSearchTermChange={term=>this.videoSearch(term)}/>
        <VideoDetail video={ this.state.selectedVideo } />
        <VideoList 
          onVideoSelect={ selectedVideo => this.setState({ selectedVideo }) } 
          videos={ this.state.videos }

        />
      </div> 
    );
  }
}

// functional component
// const App = function(){
//   return (
//     <div>
//       <SearchBar />
//     </div> 
//   );
// }

ReactDom.render (<App />, document.querySelector('.container'))