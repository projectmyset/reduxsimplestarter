import React, { Component } from 'react'

class SearchBar extends Component {

  constructor(props) {
    super(props);

    this.state = {term: ''};
  }
  render(){
    return(
      <div className='search-bar' >
        <input 
          value = { this.state.term }
          onChange={ e => this.onInputChange(e.target.value) } />
        
      </div>
      );

    

    // return <input onChange={this.onInputChange} />
    // return <input onChange={ (e) => alert(e.target.value)} />
  }

  onInputChange(term){
      this.setState({ term })
      this.props.onSearchTermChange(term);
    }
}

// const SearchBar = () => {
//   return <input />
// }

export default SearchBar;